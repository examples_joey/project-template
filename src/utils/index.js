/**
 * [AJAX]
 * 伺服器溝通
 */
export { ajax } from './ajax/ajax';

/**
 * [DATE]
 * 日期操作
 */
export { dateFormat } from './date/date-format';
export { dateChecker } from './date/date-checker';

/**
 * [DEBUG]
 * 除錯用途
 */
export { screenDebug } from './debug/screen-debug';

/**
 * [FILE]
 * 讀取檔案
 */

/**
 * [GSAP]
 * 綠襪相關
 */
export { easeLookUp } from './gsap/ease-look-up';

/**
 * [MATH]
 * 數學運算
 */
export { getRandomNumber } from './math/get-random-number';
export { getRandomNumberConfig } from './math/get-random-number-config';
export { getRandomOffsetOuter } from './math/get-random-offset-outer';
export { roundDecimal } from './math/round-decimal';
export { shuffle2DArray, shuffle2DArrayWithPos } from './math/shuffle-2d-array';

/**
 * [NODE_ELEMENT]
 * dom 操作
 */
export { brightnessBatch } from './node-element/brightness-batch';
export { clearStyleBatch } from './node-element/clear-style-batch';
export { clearStyleBatchButDisplay } from './node-element/clear-style-batch-but-display';
export { copyPasteFixed } from './node-element/copy-paste-fixed';
export { hideBatch } from './node-element/hide-batch';
export { offsetFinder } from './node-element/offset-finder';
export { removeBatch } from './node-element/remove-batch';
export { windowChecker } from './node-element/window-checker';
export { styleBatch } from './node-element/style-batch';

/**
 * [STRING_FORMAT]
 * 字串操作
 */
export { append0Left } from './string-format/append-0-left';
export { append0Right } from './string-format/append-0-right';
export { number2String } from './string-format/number-2-string';
export { splitText2Dom, splitText2DomWithoutSpace } from './string-format/split-text-2-dom';
export { string2Number } from './string-format/string-2-number';

/**
 * [URL]
 * 網址列操作
 */
export { clearAllParameter } from './url/clear-all-parameter';
export { getParameterByName } from './url/get-parameter-by-name';
export { removeParameterByName } from './url/remove-parameter-by-name';

/**
 * [GA]
 * Google Analytics 操作
 */
export { gaSender } from './ga/ga-sender';
