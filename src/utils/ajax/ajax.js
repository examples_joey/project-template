import axios from 'axios';
import querystring from 'querystring';

const ajax = async ({ method = 'get', url = '', params = {}, success = () => {}, fail = () => {}, timeout = 30000, headers = { 'Content-Type': 'application/json', 'If-Modified-Since': '0', 'X-Requested-With': 'XMLHttpRequest' }, errorAlert = true, needCheckApiStatus = true, needStringify = true }) => {
    const obj = {
        method,
        url,
        params,
        timeout,
        credentials: 'same-origin',
        headers
    };

    if (['post', 'put', 'delete'].includes(method)) {
        obj.data = needStringify ? querystring.stringify(params) : params;
    }

    return axios(obj)
        .then((res) => {
            if (res.data.code === 200 || !needCheckApiStatus) {
                success(res.data);
            }
            if (res.data.code !== 200) {
                if (res.data.message && errorAlert) {
                    alert(`${res.data.message}${res.data.code ? ` (${res.data.code})` : ''}`);
                }
                fail(res);

                /* 特例關閉 */
                switch (res.data.code) {
                    case 112211006: // 不符合活動條件(活動沒有開放給此廳主)
                    case 112211009: // 不符合活動條件(活動沒有開放此幣別)
                    case 112211010: // 不符合活動條件(測試體系不能參與活動)
                    case 112211011: // 不符合活動條件(活動狀態不可以進行系統領大獎)
                    case 112211066: // 沒有開放給廳主
                        window.close();
                        break;
                    case 112219998: // cdn_api checkcode失效
                    case 112211005: // 活動不是進行中
                    case 112212001: // 20週年-您的活動網頁資料未更新，將進行網頁重新整理以保障您活動權益。
                    case 112212003: // 同上
                    case 112212006: // 同上
                    case 112212010: // 同上
                        window.location.reload();
                        break;
                    case 112211012: // 非服務區
                        window.location.href = '/entrance/page/noservice';
                        break;
                    default:
                        break;
                }
            }
            return res.data;
        })
        .catch((error) => {
            console.error(error);
            fail(error);
        });
};

export { ajax as default };
export { ajax };
