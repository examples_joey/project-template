/**
 * 將數字加上逗號
 * @param {number} number 數字(1~n)
 * @returns {string} 返回數字加上逗號，如 10,000
 */
const number2String = (num) => {
    const number = num.toString().split('').reverse();
    for (let i = 0; i < number.length; i += 1) {
        if ((i + 1) % 4 === 0) {
            number.splice(i, 0, ',');
        }
    }
    return number.reverse().join('');
};

export { number2String as default };
export { number2String };
