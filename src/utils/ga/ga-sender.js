const gaSender = ({
    initData,
    event
}) => {
    const { ga } = window;
    const device = (initData.is_aio === 'Y') ? 'AIO' : (initData.device === 'desktop') ? 'PC' : 'mobile';
    ga('send', 'event', `#${initData.event.name}_zh_cn`, `${device}_體驗頁面_${event}`);
};

export { gaSender as default };
export { gaSender };
