/**
 * 四捨五入至小數點 Ｎ 位數
 * @param {Number} val 原始數
 * @param {Number} precision 四捨五入至小數點第 N 位
 */
const roundDecimal = (val, precision) => Math.round(Math.round(val * Math.pow(10, (precision || 0) + 1)) / 10) / Math.pow(10, (precision || 0)); // eslint-disable-line

export { roundDecimal as default };
export { roundDecimal };
