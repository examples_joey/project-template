module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": "airbnb-base",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        /* common */
        indent: [2, 4, { SwitchCase: 1 }],
        'no-unused-vars': 1,
        'comma-dangle': [2, 'never'],
        'arrow-body-style': 2,
        'global-require': 0,
        'max-len': 0,
        'no-alert': 0,
        'no-console': 0,
        "no-new": 0,
        'object-curly-newline': 0,
        'no-restricted-globals': 0,
        'no-underscore-dangle': [0, {
            'allowAfterThis': true
        }],
        camelcase: 0,

        /* import */
        'import/newline-after-import': 2,
        'import/prefer-default-export': 1,
        'import/extensions': [2, 'always', {
            js: 'never',
            vue: 'never'
        }],
        'import/no-dynamic-require': 0,
        'import/no-extraneous-dependencies': [2, {
            optionalDependencies: ['test/unit/index.js']
        }],
        'no-continue': 0,
        'linebreak-style': 0,
        'object-shorthand': 0,
        'no-nested-ternary': 0
    }
};